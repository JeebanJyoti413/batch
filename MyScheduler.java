package in.ashokit;
import java.time.LocalDateTime;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class MyScheduler {
	private static final Logger logger = LogManager.getLogger(MyScheduler.class);

	@Scheduled(cron = "0 01 21 * * 1-6")
	public void schedule() {
		logger.info("My Scheduler Works" +  LocalDateTime.now());
                                   //feature-101
	}
}
